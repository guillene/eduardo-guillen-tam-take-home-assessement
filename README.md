# TAM Take Home Assessment

This project is the take-home assessment for GitLab TAM candidates.

## Instructions

1. Using a `git` workflow of your choosing, edit this `README` file to provide answers to the questions in the [questionnaire section](#questionnaire).
1. Your commits and merge requests should be descriptive and easy to read.
1. Have fun! 😃

## Questionnaire

_Provide your answer to each question by writing you answer below it._

## What is the importance of continuous integration and continuous delivery in software development?

Continous integrations is the process of taking code from a repository and automatically building artifacts, testing the code against itself and ensuring the software built is in a ready-state and deliverable. 

Continous delivery is the automated process of taking code made in small iterations and releasing the code to different environments. The importance of continous integration and continous delivery (CI/CD) in software development is that CI/CD vastly improves the deployment pipeline effiency. 

#### The main advantages for CI/CD tools include:

- Speed up deployment and ability to deploy often, fix bugs and add new features

- Automation of test and deployment process to reduce errors

- Iteration capabilities 

- Reduce costs by spending less time debugging problems 

- Consistency of deployments across environments

- Minimal downtime risk

## What are the advantages and disadvantages of using git?

Git is defined as the most popular Version Control System in the world. Git records the changes made to code in a database called a repository. With git, you can look at the history and changes made to the code and revert to previous versions. Git makes it possible for multiple developers to collaborate more efficiently.

#### Advantages

- Easily track and store copies of entire projects. 

- Developers can collaborate more efficiently. 

- Scalable, free, and open-source

#### Disadvantages

- If the git is centralized, it's challenging to collaborate if the server where the repository is offline.

- A steep learning curve, there are many commands and options. 

- A large number of binary files can make git slow. 


## What quick action would you use in a GitLab merge request comment to assign a merge request to a user? Please assign your merge request for your answers to the user provided in your assessment guidance.

The quick action to assign a merge request to another user is under the "Assignees" tab in the new merge request page.

## What are the basic hardware requirements we recommend for installing GitLab?

### Hardware requirements
 
## Storage

The necessary hard drive space largely depends on the size of the repositories you want to store in GitLab but as a rule of thumb you should have at least as much free space as all your repositories combined take up.

If you want to be flexible about growing your hard drive space in the future consider mounting it using logical volume management (LVM) so you can add more hard drives when you need them.

Apart from a local hard drive you can also mount a volume that supports the network file system (NFS) protocol. This volume might be located on a file server, a network-attached storage (NAS) device, a storage area network (SAN) or on an Amazon Web Services (AWS) Elastic Block Store (EBS) volume.

If you have enough RAM and a recent CPU the speed of GitLab is mainly limited by hard drive seek times. Having a fast drive (7200 RPM and up) or a solid state drive (SSD) improves the responsiveness of GitLab.

## CPU

CPU requirements are dependent on the number of users and expected workload. Your exact needs may be more, depending on your workload. Your workload is influenced by factors such as - but not limited to - how active your users are, how much automation you use, mirroring, and repository/change size.
The following is the recommended minimum CPU hardware guidance for a handful of example GitLab user base sizes.

4 cores is the recommended minimum number of cores and supports up to 500 users
8 cores supports up to 1000 users
More users? Consult the reference architectures page

## Memory

Memory requirements are dependent on the number of users and expected workload. Your exact needs may be more, depending on your workload. Your workload is influenced by factors such as - but not limited to - how active your users are, how much automation you use, mirroring, and repository/change size.
The following is the recommended minimum Memory hardware guidance for a handful of example GitLab user base sizes.

4GB RAM is the required minimum memory size and supports up to 500 users
Our Memory Team is working to reduce the memory requirement.
8GB RAM supports up to 1000 users

More users? Consult the reference architectures page
In addition to the above, we generally recommend having at least 2GB of swap on your server, even if you currently have enough available RAM. Having swap helps to reduce the chance of errors occurring if your available memory changes. We also recommend configuring the kernel’s swappiness setting to a low value like 10 to make the most of your RAM while still having the swap available when needed.


## What are the six core values of GitLab (the company)?

#### CREDIT

GitLabs six core values are: 

- Collaboration 

- Results

- Efficiency

- Diversity & Belonging

- Iteration

- Transperancy 


# References

- https://www.linkedin.com/pulse/cicd-what-why-important-rob-larter/

- https://www.youtube.com/watch?v=2ReR1YJrNOM

- https://www.quora.com/What-are-the-advantages-and-disadvantages-of-using-Git

- https://docs.gitlab.com/ee/install/requirements.html#hardware-requirements

- https://about.gitlab.com/handbook/values/
